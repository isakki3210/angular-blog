import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, RouterModule, NavbarComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent {
  public blogData: any[] = [
    {
      id: 1,
      title: 'The Importance of Goal Setting',
      description:
        'Learn why setting goals is crucial for personal and professional success, and practical tips on how to set achievable goals.',
      imagePath: 'assets/images/goal.jpg',
    },
    {
      id: 2,
      title: 'Effective Time Management Techniques',
      description:
        'Discover proven strategies to manage your time effectively, improve productivity, and reduce stress.',
      imagePath: 'assets/images/time.jpg',
    },
    {
      id: 3,
      title: 'Introduction to Machine Learning',
      description:
        'Explore the basics of machine learning, its applications in various industries, and how it is shaping the future of technology.',
      imagePath: 'assets/images/machinelearning.png',
    },
    {
      id: 4,
      title: 'Healthy Eating Habits for a Balanced Life',
      description:
        'Learn about the importance of nutrition, tips for maintaining a balanced diet, and how to make healthier food choices.',
      imagePath: 'assets/images/eatinghabits.jpg',
    },
    {
      id: 5,
      title: 'The Art of Negotiation',
      description:
        'Master the art of negotiation with strategies and techniques to achieve win-win outcomes in business and personal interactions.',
      imagePath: 'assets/images/negotiation.jpg',
    },
    {
      id: 6,
      title: 'Tips for Effective Communication',
      description:
        'Improve your communication skills with practical tips on active listening, non-verbal cues, and building rapport.',
      imagePath: 'assets/images/communication.jpg',
    },
  ];
}
