import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router, RouterOutlet } from '@angular/router';
import { User } from './user.model';
import { LoginService } from './login.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    HttpClientModule,
    RouterOutlet,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
  ],
  providers: [LoginService],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent implements OnInit {
  /** Use to Get from the user while using ngModel*/
  public emailid: string = '';
  public password: string = '';

  /** To Validate..*/
  private pass: string = '123';
  private email: string = 'esakki3210@gmail.com';

  public result: string = '';
  public resClass: boolean = false;
  public resDNone: boolean = false;
  public loginForm: FormGroup;

  constructor(private router: Router, private loginService: LoginService) {
    this.loginForm = new FormGroup({
      emailValidation: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      passValidation: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15),
      ]),
    });
  }
  ngOnInit(): void {}

  login() {
    // console.log(this.emailid);
    // console.log(this.username);
    // console.log(this.password);

    const user: User = {
      email: this.loginForm.get('emailValidation')?.value,
      phone: '',
      username: '',
      userpass: this.loginForm.get('passValidation')?.value,
      active: 'Y',
    };

    if (this.loginForm.valid) {
      this.loginService.login(user).subscribe((loggedIn) => {
        if (loggedIn) {
          this.resDNone = true;
          this.resClass = true;
          this.result = 'Login Success..';
          setTimeout(() => {
            this.router.navigateByUrl('/home');
          }, 1000);
        } else {
          this.resDNone = true;
          this.resClass = false;
          this.result = 'Login Failed..';
        }
      });

      // if (
      //   this.loginForm.get('emailValidation')?.value === this.email &&
      //   this.loginForm.get('passValidation')?.value === this.pass
      // ) {
      //   this.resDNone = true;
      //   this.resClass = true;
      //   this.result = 'Login Success..';
      //   setTimeout(() => {
      //     this.router.navigateByUrl('/home');
      //   }, 1000);
      // } else {
      //   this.resDNone = true;
      //   this.resClass = false;
      //   this.result = 'Login Failed..';
      // }
    } else {
      if (this.loginForm.get('emailValidation')?.invalid) {
        this.resDNone = true;
        this.resClass = false;
        this.result = 'Enter Valid Email Id..';
      } else if (this.loginForm.get('passValidation')?.invalid) {
        this.resDNone = true;
        this.resClass = false;
        this.result = 'Enter Valid Password..';
      }
    }
  }
}
