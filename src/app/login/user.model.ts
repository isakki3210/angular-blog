export interface User {
  email: string;
  username: string;
  phone: string;
  userpass: string;
  active: string;
}
