import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './user.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
  useClass: HttpClientModule,
})
export class LoginService {
  private baseUrl = 'http://localhost:8080/auth';

  constructor(private http: HttpClient) {}

  login(user: any): Observable<boolean> {
    return this.http.post<boolean>(`${this.baseUrl}/login`, user);
  }
}
